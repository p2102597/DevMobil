package com.example.tp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tp.model.MaBaseSQLite;
import com.example.tp.model.UserData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final static int REQUEST_FORM =250;

    FloatingActionButton bt_Add ;
    UsersAdapter usersAdapter;
    ListView lv;

    ArrayList<UserData> listData = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initListUser();


        usersAdapter = new UsersAdapter(listData,this);
        lv = findViewById(R.id.lv_contact);

        bt_Add= findViewById(R.id.bt_add);

        lv.setAdapter(usersAdapter);

        bt_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,FormActivity.class);
                i.putExtra(FormActivity.KEY_FORM,new UserData());
                startActivityForResult(i,REQUEST_FORM);
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MainActivity.this, listData.get(position).name, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(MainActivity.this,DetailActivity.class);
                i.putExtra(DetailActivity.KEY_USER,listData.get(position));

                startActivity(i);
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                remove(position);
                return true;
            }
        });

    }

    private void remove(int position){
        MaBaseSQLite maBase = new MaBaseSQLite(this);
        SQLiteDatabase db = maBase.getWritableDatabase();
        String whereClause = "id=?";
        String[] whereArgs = new String[] { String.valueOf(listData.get(position).id) };
        db.delete("users", whereClause, whereArgs);
        listData.remove(position);
        usersAdapter.notifyDataSetChanged();
    }

    private void initListUser(){
        MaBaseSQLite maBase = new MaBaseSQLite(this);
        SQLiteDatabase db = maBase.getReadableDatabase();
        Cursor cursor = db.query("users", null, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String prenom = cursor.getString(cursor.getColumnIndex("prenom"));
                String dateNaissance = cursor.getString(cursor.getColumnIndex("dateNaissance"));
                String tel = cursor.getString(cursor.getColumnIndex("tel"));
                String mail = cursor.getString(cursor.getColumnIndex("mail"));
                String codePostal = cursor.getString(cursor.getColumnIndex("codePostal"));
                String ville = cursor.getString(cursor.getColumnIndex("ville"));
                String genre = cursor.getString(cursor.getColumnIndex("genre"));


                UserData user_tmp= new UserData();
                user_tmp.id=id;
                user_tmp.name=name;
                user_tmp.prenom=prenom;
                user_tmp.dateNaissance=dateNaissance;
                user_tmp.tel=tel;
                user_tmp.mail=mail;
                user_tmp.codePostal=codePostal;
                user_tmp.ville=ville;
                user_tmp.genre=genre;
                listData.add(user_tmp);

            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    private String saveImageToInternalStorage(Uri imageUri, String filename) {
        String filePath = null;
        try {
            InputStream inputStream = getContentResolver().openInputStream(imageUri);
            File dir = getApplicationContext().getDir("images", Context.MODE_PRIVATE); // Create directory in internal storage
            File file = new File(dir, filename); // Create file with generated filename
            filePath = file.getAbsolutePath(); // Get absolute path of file
            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filePath;
    }

    private void addUser(UserData userData){
        listData.add(userData);
        MaBaseSQLite maBase = new MaBaseSQLite(this);
        SQLiteDatabase db = maBase.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", userData.name);
        values.put("prenom", userData.prenom);
        values.put("dateNaissance", userData.dateNaissance);
        values.put("tel", userData.tel);
        values.put("mail", userData.mail);
        values.put("codePostal", userData.codePostal);
        values.put("ville", userData.ville);
        values.put("genre", userData.genre);

        long rowId = db.insert("users", null, values);
        saveImageToInternalStorage(Uri.parse(userData.img),"avatar_"+rowId);
        userData.id= (int) rowId;
        usersAdapter.notifyDataSetChanged();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            if(requestCode== REQUEST_FORM && data!=null){
                UserData user = (UserData) data.getSerializableExtra(FormActivity.KEY_FORM);
                addUser(user);
                //Toast.makeText(this, user.prenom, Toast.LENGTH_LONG).show();
            }
        }else{
        }


    }
}