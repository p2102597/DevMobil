package com.example.tp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.tp.model.MaBaseSQLite;
import com.example.tp.model.UserData;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FormActivity extends AppCompatActivity {

    public static String KEY_FORM = "form";
    final static int REQUEST_DETAIL =237;
    public static final int PICK_IMAGE = 1;
    static String ERROR ="le champ est vide";

    RadioButton radio_Homme;
    RadioButton radio_femme;
    RadioButton radio_autre;
    RadioGroup radioGroup;

    TextInputEditText input_name;
    TextInputEditText input_Prenom;
    TextInputEditText input_dateAniv;
    TextInputEditText input_tel;
    TextInputEditText input_mail;
    TextInputEditText input_codePostal;
    TextInputEditText input_ville;

    String image = "android.resource://com.example.tp/drawable/pp_bleu";

    ImageView iv_avatar;

    final Calendar myCalendar= Calendar.getInstance();





    Button bt_validate;

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        bt_validate = findViewById(R.id.bt_validate);

        input_name= findViewById(R.id.input_name);
        input_Prenom= findViewById(R.id.input_Prenom);
        input_tel= findViewById(R.id.input_tel);
        input_mail= findViewById(R.id.input_mail);
        input_codePostal= findViewById(R.id.input_codePostal);
        input_ville= findViewById(R.id.input_ville);

        radio_Homme= findViewById(R.id.radio_Homme);
        radio_femme= findViewById(R.id.radio_femme);
        radio_autre= findViewById(R.id.radio_autre);

        iv_avatar = findViewById(R.id.iv_avatar);

        radioGroup=findViewById(R.id.radioGroup);




        input_dateAniv=findViewById(R.id.input_dateAniv);
        DatePickerDialog.OnDateSetListener date =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateLabel();
            }
        };
        input_dateAniv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(FormActivity.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        bt_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Detail();
            }
        });

        iv_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });

    }
    private void updateLabel(){
        String myFormat="MM/dd/yy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.FRANCE);
        input_dateAniv.setText(dateFormat.format(myCalendar.getTime()));
    }

    private void Detail(){
        String nom = input_name.getText().toString();
        String prenom= input_Prenom.getText().toString();
        String dateNaissance = input_dateAniv.getText().toString();
        String tel=input_tel.getText().toString();
        String mail=input_mail.getText().toString();
        String codePostal=input_codePostal.getText().toString();
        String ville= input_ville.getText().toString();
        String genre ="";
        if(radioGroup.getCheckedRadioButtonId()!=-1){
            genre =((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
        }


        if(nom.isEmpty()){
            input_name.setError(ERROR);
            return;
        }
        if(prenom.isEmpty()){
            input_Prenom.setError(ERROR);
            return;
        }
        if(tel.isEmpty()){
            input_tel.setError(ERROR);
            return;
        }
        UserData user = new UserData();
        user.name=nom;
        user.prenom=prenom;
        user.dateNaissance=dateNaissance;
        user.tel=tel;
        user.mail=mail;
        user.codePostal=codePostal;
        user.ville=ville;
        user.genre=genre;
        user.img=image;
        //Intent i = new Intent(FormActivity.this,DetailActivity.class);
        //i.putExtra(DetailActivity.KEY_USER,user);

        Intent i = new Intent();
        i.putExtra(KEY_FORM,user);
        setResult(RESULT_OK,i);
        finish();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE) {
                Uri imageUri = data.getData();
                iv_avatar.setImageURI(imageUri);
                image = imageUri.toString();
            }
        }
    }

}