package com.example.tp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tp.model.UserData;

import java.io.File;

public class DetailActivity extends AppCompatActivity {

    final static String KEY_USER= "user";

    private Button btFinish;
    private UserData user;
    private ImageView iv_avatar_det;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView tv_name=findViewById(R.id.tv_name);
        TextView ty_prenom=findViewById(R.id.tv_prenom);
        TextView tv_autre=findViewById(R.id.tv_autre);
        btFinish = findViewById(R.id.btFinish);
        iv_avatar_det = findViewById(R.id.iv_avatar_det);

        user = (UserData) getIntent().getSerializableExtra(KEY_USER);

        tv_name.setText(user.name);
        ty_prenom.setText(user.prenom);
        tv_autre.setText(user.getAutre());

        File imgFile = new File(getDir("images", Context.MODE_PRIVATE), "avatar_" + user.id);
        Uri imgUri = Uri.fromFile(imgFile);
        iv_avatar_det.setImageURI(imgUri);


        btFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra(KEY_USER,"marche");
                setResult(RESULT_OK,i);
                finish();
            }
        });

    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra(KEY_USER,"annule");
        setResult(RESULT_CANCELED,i);
        super.onBackPressed();
    }
}