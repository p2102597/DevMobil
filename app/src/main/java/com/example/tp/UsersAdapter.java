package com.example.tp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.tp.model.UserData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class UsersAdapter extends BaseAdapter {

    ArrayList<UserData> listUsers;
    Context context;

    public UsersAdapter(ArrayList<UserData> listUsers , Context context){
        this.listUsers=listUsers;
        this.context=context;
    }
    private void loadImageFromStorage(String path, ImageView imageView) {
        try {
            File file = new File(path);
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
            imageView.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getCount() {
        return listUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return listUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ConstraintLayout layout;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if(convertView==null){
            layout = (ConstraintLayout) inflater.inflate(R.layout.adapter_user,parent,false);
        }else{
            layout= (ConstraintLayout) convertView;
        }

        ImageView iv_avatar = layout.findViewById(R.id.iv_avatar_cont);
        //loadImageFromStorage(context.getFilesDir().toString(),iv_avatar);
        Log.e("TAG", context.getFilesDir().toString() );
        File imgFile = new File(context.getDir("images", Context.MODE_PRIVATE), "avatar_" + listUsers.get(position).id);
        Uri imgUri = Uri.fromFile(imgFile);
        iv_avatar.setImageURI(imgUri);

        TextView tv_name = layout.findViewById(R.id.tv_first_name_cont);
        tv_name.setText(listUsers.get(position).name+" "+listUsers.get(position).prenom);

        return layout;
    }
}
