package com.example.tp.model;

import android.net.Uri;

import java.io.Serializable;

public class UserData implements Serializable {
    public int id;
    public String name;
    public String prenom;
    public String dateNaissance;
    public String tel;
    public String mail;
    public String codePostal;
    public String ville;
    public String genre;
    public String img;

    public String getAutre(){
        String tmp="";
        tmp+=dateNaissance+"\n";
        tmp+=tel+"\n";
        tmp+=mail+"\n";
        tmp+=codePostal+"\n";
        tmp+=ville+"\n";
        tmp+=genre;
        return tmp;
    }
}
