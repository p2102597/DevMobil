package com.example.tp.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MaBaseSQLite extends SQLiteOpenHelper {

    private static final String TABLE_NAME = "users";
    private static final String COL_NAME = "name";
    private static final String COL_PRENOM = "prenom";
    private static final String COL_DATE_NAISSANCE = "dateNaissance";
    private static final String COL_TEL = "tel";
    private static final String COL_MAIL = "mail";
    private static final String COL_CODE_POSTAL = "codePostal";
    private static final String COL_VILLE = "ville";
    private static final String COL_GENRE = "genre";
    private static final String COL_IMG = "img";

    private static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COL_NAME + " TEXT," +
                    COL_PRENOM + " TEXT," +
                    COL_DATE_NAISSANCE + " TEXT," +
                    COL_TEL + " TEXT," +
                    COL_MAIL + " TEXT," +
                    COL_CODE_POSTAL + " TEXT," +
                    COL_VILLE + " TEXT," +
                    COL_GENRE + " TEXT );";

    public MaBaseSQLite(Context context) {
        super(context, "users.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Ici vous pouvez gérer les changements de schéma
    }
}
